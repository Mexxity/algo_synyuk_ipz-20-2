﻿# include <iostream>
# include <time.h>

struct date
{
	unsigned short WeekDay : 3;
	unsigned short MonthDay : 6;
	unsigned short Month : 4;
	unsigned short Year : 11;
	unsigned short Hours : 5;
	unsigned short Minutes : 6;
	unsigned short Seconds : 6;
};


int main()
{
	date d = { 03, 04, 03, 2003, 13, 33, 00 };
	printf("Date:\nHour- %d, \nMinutes- %d, \nSecond- %d, \nYear- %d, \nMonth- %d, \nMonthDay- %d, \nWeekday- %d\n", d.Hours, d.Minutes, d.Seconds, d.Year, d.Month, d.MonthDay, d.WeekDay);
	return 0;
}
