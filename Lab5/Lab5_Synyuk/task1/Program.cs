﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace Sorting
{
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
        }
        public T Data { get; set; }
        public Node<T> Previous { get; set; }
        public Node<T> Next { get; set; }
    }
    public class LinkedList<T> : IEnumerable<T>
    {
        Node<T> head;
        Node<T> tail;
        int count;
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            count++;
        }
        public void Add(Node<T> node)
        {
            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            count++;
        }
        public void AddFirst(T data)
        {
            Node<T> node = new Node<T>(data);
            Node<T> temp = head;
            node.Next = temp;
            head = node;
            if (count == 0)
                tail = head;
            else
                temp.Previous = node;
            count++;
        }





        public void Remove(Node<T> data)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current == data)
                    break;
                current = current.Next;
            }
            if (current != null)
            {

                if (current.Next != null)
                    current.Next.Previous = current.Previous;
                else
                    tail = current.Previous;
                if (current.Previous != null)
                    current.Previous.Next = current.Next;
                else
                    head = current.Next;
                count--;
            }
        }
        public Node<T> GetTail()
        {
            return tail;
        }
        public Node<T> GetHead()
        {
            return head;
        }
        public void SetHead(Node<T> node)
        {
            Node<T> temp = head;
            node.Next = temp;
            head = node;
            temp.Previous = node;
        }
        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }
        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

    }








    class Program
    {
        static void Sort_by_selection_list(Node<int> head)
        {
            Node<int> temp = head;
            //Прохождение по списку
            while (temp != null)
            {
                Node<int> min = temp;
                Node<int> after = temp.Next;
                //Обход несортированного подсписка
                while (after != null)
                {
                    if (min.Data > after.Data)
                        min = after;

                    after = after.Next;
                }
                //Обмен элементов
                int x = temp.Data;
                temp.Data = min.Data;
                min.Data = x;
                temp = temp.Next;
            }
        }
        static void Sort_by_inserts_array(int[] arr)
        {
            //Перебор элементов в неотсортированной части массива
            for (int i = 1; i < arr.Length; i++)
            {
                int j;
                int tmp = arr[i];
                //Каждый элемент вставляется в отсортированную часть массива и упорядочивается таким образом
                for (j = i - 1; j >= 0; j--)
                {
                    if (arr[j] < tmp)
                        break;
                    arr[j + 1] = arr[j];
                }
                //Ставим tmp перед найденным элементом
                arr[j + 1] = tmp;
            }
        }
        public class LinkedListSecond
        {
            public node head;
            public node sorted;
            int count;
            public class node
            {
                public int val;
                public node next;
                public node(int val)
                {
                    this.val = val;
                }
            }
            //Вставка элементов в список
            public void push(int val)
            {
                node newnode = new node(val);
                newnode.next = head;
                head = newnode;
            }



            public void insertionList(node headref)
            {
                //Создаем первоначальный список
                sorted = null;
                node current = headref;
                //Прохождение по списку и внесения current в отсортированный список
                while (current != null)
                {
                    node next = current.next;
                    sortedInsert(current);
                    current = next;
                }
                //Меняем ссылку на первый элемент
                head = sorted;
            }
            //Метод для вставки элемента в список
            public void sortedInsert(node newnode)

            {
                // Перенос элемента список в начало, если он min
                if (sorted == null || sorted.val >= newnode.val)
                {
                    newnode.next = sorted;
                    sorted = newnode;
                }
                else
                {
                    node current = sorted;
                    //Поиск элемента, перед которым нужно вставить узел списка
                    while (current.next != null &&
                            current.next.val < newnode.val)
                    {
                        current = current.next;
                    }
                    //Изменение ссылок на сусдни элементы от найденного
                    newnode.next = current.next;
                    current.next = newnode;
                }
            }
            public void сlear()
            {
                head = null;
                count = 0;
            }
        }
        static void Main(string[] args)
        {
            Random random = new Random();
            Stopwatch st;

            Console.WriteLine("Sort by selection (list)");
            LinkedList<int> linkedList = new LinkedList<int>();
            Node<int> head;


            for (int i = 0; i < 10; i++)
                linkedList.Add(random.Next());

            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            Sort_by_selection_list(head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(10 items) - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();




            for (int i = 0; i < 100; i++)
                linkedList.Add(random.Next());
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            Sort_by_selection_list(head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(100 items)  - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();

            for (int i = 0; i < 500; i++)
                linkedList.Add(random.Next());
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            Sort_by_selection_list(head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(500 items)  - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();

            for (int i = 0; i < 1000; i++)
                linkedList.Add(random.Next());
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            Sort_by_selection_list(head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(1000 items)  - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();

            for (int i = 0; i < 2000; i++)
                linkedList.Add(random.Next());
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            Sort_by_selection_list(head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(2000 items)  - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();

            for (int i = 0; i < 5000; i++)
                linkedList.Add(random.Next());
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            Sort_by_selection_list(head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(5000 items)  - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();

            for (int i = 0; i < 10000; i++)
                linkedList.Add(random.Next());
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            Sort_by_selection_list(head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(10000 items) - {st.Elapsed.TotalMilliseconds}");
            Console.WriteLine("\n");

            Console.WriteLine("Sort by inserts (array)");
            int[] arr = new int[10];

            for (int i = 0; i < arr.Length; i++)
                arr[i] = random.Next();




            st = Stopwatch.StartNew();
            Sort_by_inserts_array(arr);
            st.Stop();
            Console.WriteLine($"Array sorting time(10 elements) - {st.Elapsed.TotalMilliseconds}");

            arr = new int[100];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = random.Next();
            st = Stopwatch.StartNew();
            Sort_by_inserts_array(arr);
            st.Stop();
            Console.WriteLine($"Array sorting time(100 elements)  - {st.Elapsed.TotalMilliseconds}");

            arr = new int[500];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = random.Next();
            st = Stopwatch.StartNew();
            Sort_by_inserts_array(arr);
            st.Stop();
            Console.WriteLine($"Array sorting time(500 elements)  - {st.Elapsed.TotalMilliseconds}");

            arr = new int[1000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = random.Next();
            st = Stopwatch.StartNew();
            Sort_by_inserts_array(arr);
            st.Stop();
            Console.WriteLine($"Array sorting time(1000 elements)  - {st.Elapsed.TotalMilliseconds}");

            arr = new int[2000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = random.Next();
            st = Stopwatch.StartNew();
            Sort_by_inserts_array(arr);
            st.Stop();
            Console.WriteLine($"Array sorting time(2000 elements)  - {st.Elapsed.TotalMilliseconds}");

            arr = new int[5000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = random.Next();
            st = Stopwatch.StartNew();
            Sort_by_inserts_array(arr);
            st.Stop();
            Console.WriteLine($"Array sorting time(5000 elements) - {st.Elapsed.TotalMilliseconds}");

            arr = new int[10000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = random.Next();
            st = Stopwatch.StartNew();
            Sort_by_inserts_array(arr);
            st.Stop();
            Console.WriteLine($"Array sorting time(10000 elements) - {st.Elapsed.TotalMilliseconds}");
            Console.WriteLine("\n");


            Console.WriteLine("Sort by inserts (list)");
            LinkedListSecond list = new LinkedListSecond();




            for (int i = 0; i < 10; i++)
                list.push(random.Next());
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(10 items) - {st.Elapsed.TotalMilliseconds}");

            list.сlear();
            for (int i = 0; i < 100; i++)
                list.push(random.Next());
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(100 items) - {st.Elapsed.TotalMilliseconds}");

            list.сlear();
            for (int i = 0; i < 500; i++)
                list.push(random.Next());
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(500 items) - {st.Elapsed.TotalMilliseconds}");

            list.сlear();
            for (int i = 0; i < 1000; i++)
                list.push(random.Next());
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(1000 items) - {st.Elapsed.TotalMilliseconds}");

            list.сlear();
            for (int i = 0; i < 2000; i++)
                list.push(random.Next());
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(2000 items) - {st.Elapsed.TotalMilliseconds}");

            list.сlear();
            for (int i = 0; i < 5000; i++)
                list.push(random.Next());
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(5000 items) - {st.Elapsed.TotalMilliseconds}");

            list.сlear();
            for (int i = 0; i < 10000; i++)
                list.push(random.Next());
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Time to sort the list(10000 items) - {st.Elapsed.TotalMilliseconds}");
            Console.ReadKey();
        }
    }
}
